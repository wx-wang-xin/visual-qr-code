package com.visual.qr.test;

import com.visual.qr.QrCodeBuilder;

import java.awt.*;

public class Test {

    public static void main(String[] args) {
        QrCodeBuilder builder = new QrCodeBuilder.Builder("www测试二维码www").builder();
        String url = "https://img0.baidu.com/it/u=2759020622,38917165&fm=253&fmt=auto&app=138&f=JPEG?w=800&h=500";
        System.out.println(builder.CreateStreamDefineBgImgNetQrCode(url, Color.DARK_GRAY, 100, 200));
    }

}
