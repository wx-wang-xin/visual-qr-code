package com.visual.qr;
import org.apache.commons.lang3.ObjectUtils;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.visual.qr.config.MatrixToImageWriterConfig;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Base64;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * 二维码生成工具
 */
public class QrCodeBuilder {

    /** 二维码内容 **/
    private String content;

    /** 二维码宽度 **/
    private Integer width;

    /** 二维码高度 **/
    private Integer height;

    /** 二维码图片类型 **/
    private String type;

    /** 二维码图片logo **/
    private String logo;

    private QrCodeBuilder(Builder builder) {
        this.content = builder.content;
        this.width = builder.width;
        this.height = builder.height;
        this.type = builder.type;
    }

    public static class Builder {

        /** 二维码内容 **/
        private String content;

        /** 二维码宽度 **/
        private Integer width;

        /** 二维码高度 **/
        private Integer height;

        /** 二维码图片类型 **/
        private String type;

        /** 二维码图片logo **/
        private String logo;

        public Builder(String content) {
            this.content = content;
            this.width = 300;
            this.height = 300;
            this.type = "jpg";
        }

        public QrCodeBuilder builder() {
            return new QrCodeBuilder(this);
        }

        /**
         * 设置二维码宽度
         * @param width
         * @return
         */
        public Builder setWidth(int width) {
            this.width = width;
            return this;
        }

        /**
         * 设置二维码高度
         * @param height
         * @return
         */
        public Builder setHeight(int height) {
            this.height = height;
            return this;
        }

        /**
         * 设置二维码图片类型
         * @param type
         * @return
         */
        public Builder setType(String type) {
            if (type != null && !type.equals("")) {
                this.type = type;
            } else {
                this.type = "jpg";
            }
            return  this;
        }

        /**
         * 设置二维码logo
         * @param logo
         * @return
         */
        public Builder setLogo(String logo) {
            this.logo = logo;
            return this;
        }
    }

    /**
     * 生成普通二维码（base64格式）
     * @return data:image/png;base64, + base64的二维码字符串
     */
    public String CreateQrCode() throws IOException {
        if (ObjectUtils.isEmpty(this.content)) { throw new IllegalArgumentException("二维码内容不能为空"); }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        String auth_url = null;
        try {
            Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
            hints.put(EncodeHintType.MARGIN, 0);
            BitMatrix bitMatrix = new MultiFormatWriter().encode(this.content, BarcodeFormat.QR_CODE, this.width, this.height, hints);
            MatrixToImageWriter.writeToStream(bitMatrix, this.type, outputStream);
            auth_url = Base64.getEncoder().encodeToString(outputStream.toByteArray());
            return "data:image/png;base64," + auth_url;
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("二维码生成失败:" + e.getMessage());
        } finally {
            outputStream.flush();
            outputStream.close();
        }
    }

    /**
     * 生成二维码图片(返回流)
     * @return InputStream
     */
    public InputStream CreateStreamQrCode() throws IOException {
        if (ObjectUtils.isEmpty(this.content)) { throw new IllegalArgumentException("二维码内容不能为空"); }
        // 创建一个输出流
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        InputStream is = null;
        try {
            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            Map<EncodeHintType, String> hints = new HashMap<EncodeHintType, String>();
            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
            BitMatrix bitMatrix = multiFormatWriter.encode(this.content, BarcodeFormat.QR_CODE, this.width, this.height, hints);
            // 将二维码放入缓冲流
            BufferedImage image = MatrixToImageWriterConfig.toBufferedImage(bitMatrix);
            for (int i = 0; i < 400; i++) {
                for (int j = 0; j < 400; j++) {
                    //4、循环将二维码内容定入图片
                    image.setRGB(i, j, bitMatrix.get(i, j) ? 0xFF000000 : 0xFFFFFFFF);
                }
            }
            //将图片写出到指定位置（复制图片）
            ImageIO.write(image, this.type, os);
            is = new ByteArrayInputStream(os.toByteArray());
            return is; // 返回流
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("二维码生成失败:" + e.getMessage());
        } finally {
            os.close();
            if (is != null) {
                is.close();
            }
        }
    }

    /**
     * 创建有背景图片的二维码(输出base64)
     * @param bgUrl 可以为url可以为本地路径
     * @return data:image/png;base64, + base64的二维码字符串
     */
    public String CreateNetBgImgNetQrCode(String bgUrl, Color color) {
        try {
            return getStreamToBase64Qr(VisualQRCode.createQRCode(
                    this.content,
                    bgUrl,
                    'H',
                    color,
                    null,
                    VisualQRCode.POSITION_DETECTION_SHAPE_MODEL_RECTANGLE,
                    VisualQRCode.FILL_SHAPE_MODEL_RECTANGLE));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 创建有背景图片的二维码(输出base64)
     * @param bgUrl 可以为url可以为本地路径
     * @return data:image/png;base64, + base64的二维码字符串
     */
    public String CreateNetBgImgNetQrCode(String bgUrl) {
        try {
            return getStreamToBase64Qr(VisualQRCode.createQRCode(
                    this.content,
                    bgUrl,
                    'H',
                    Color.DARK_GRAY,
                    null,
                    VisualQRCode.POSITION_DETECTION_SHAPE_MODEL_RECTANGLE,
                    VisualQRCode.FILL_SHAPE_MODEL_RECTANGLE));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取输入流转base64格式图片
     * @param inputStream
     * @return
     * @throws IOException
     */
    private String getStreamToBase64Qr(InputStream inputStream) throws IOException {
        // 读取输入流中的图片数据到字节数组
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096];
        int bytesRead;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
        byte[] imageBytes = outputStream.toByteArray();
        // 使用Base64编码将字节数组转换为BASE64格式的字符串
        return "data:image/png;base64," + Base64.getEncoder().encodeToString(imageBytes);
    }

    /**
     * 创建有背景图片的二维码(输出流)
     * @param bgUrl 可以为url可以为本地路径
     */
    public InputStream CreateStreamNetBgImgNetQrCode(String bgUrl) {
        try {
            return VisualQRCode.createQRCode(
                    this.content,
                    bgUrl,
                    'H',
                    Color.DARK_GRAY,
                    null,
                    VisualQRCode.POSITION_DETECTION_SHAPE_MODEL_RECTANGLE,
                    VisualQRCode.FILL_SHAPE_MODEL_RECTANGLE);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 创建有背景图片的二维码(输出流)
     * @param bgUrl 可以为url可以为本地路径
     * @param color
     */
    public InputStream CreateStreamNetBgImgNetQrCode(String bgUrl, Color color) {
        try {
            return VisualQRCode.createQRCode(
                    this.content,
                    bgUrl,
                    'H',
                    color,
                    null,
                    VisualQRCode.POSITION_DETECTION_SHAPE_MODEL_RECTANGLE,
                    VisualQRCode.FILL_SHAPE_MODEL_RECTANGLE);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 创建有背景图片的二维码(自定义二维码位置 输出流)
     * @param bgUrl 可以为url可以为本地路径
     */
    public InputStream CreateStreamDefineBgImgNetQrCode(
            String bgUrl,
            Integer startX,
            Integer startY) {
        try {
            return VisualQRCode.createQRCode(
                    this.content,
                    bgUrl,
                    'H',
                    Color.DARK_GRAY,
                    this.width,
                    startX,
                    startY,
                    VisualQRCode.POSITION_DETECTION_SHAPE_MODEL_RECTANGLE,
                    VisualQRCode.FILL_SHAPE_MODEL_RECTANGLE);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 创建有背景图片的二维码(自定义二维码位置 base64格式)
     * @param bgUrl 可以为url可以为本地路径
     * @param color
     */
    public String CreateStreamDefineBgImgNetQrCode(String bgUrl,
                                                        Color color,
                                                        Integer startX,
                                                        Integer startY) {
        try {
            return getStreamToBase64Qr(VisualQRCode.createQRCode(
                    this.content,
                    bgUrl,
                    'H',
                    color,
                    this.width,
                    startX,
                    startY,
                    VisualQRCode.POSITION_DETECTION_SHAPE_MODEL_RECTANGLE,
                    VisualQRCode.FILL_SHAPE_MODEL_RECTANGLE));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
